# runners.rb
require 'rubygems'
require 'data_mapper'
require 'json'
require 'date'

DataMapper::Logger.new(STDOUT, :debug)
DataMapper.setup(:default, 'mysql://root:chief@localhost/runner')

class Runner
	include DataMapper::Resource

	property :id,         Serial    		
 	property :sex, 		 Enum[ :M, :F ] 	
	property :dob, 		 Date ,:required => false
	
	has n, :results
	has n, :runnernames
	has n, :races, :through => :results

	def to_json(*a)
		{
		  'runner_id' => self.id,
		  'firstname' => self.runnernames.first().firstname,
		  'lastname' => self.runnernames.first().lastname,
		  'sex' => self.sex
		}.to_json(*a)
	end 

end

class Runnername
	include DataMapper::Resource

	property :id,      Serial 
	property :firstname,  String    		
	property :lastname,   String  
	belongs_to :runner

	def to_json(*a)
		{
		  'runnername_id' => self.id,
		  'firstname' => self.firstname,
		  'lastname' => self.lastname
		}.to_json(*a)
	end 

end 

class Result
	include DataMapper::Resource
	
	property :id,      Serial 
	property :bibnumber,  	String    		
	property :time,   		Integer 

	belongs_to :runner, :required => true		#The runner	
	belongs_to :race, :required => true		#The race that the result is for.
	belongs_to :club, :required => false	

	def to_json(*a)
		{
		  'bibnumber' => self.bibnumber,
 		  'hours' => self.time / (60 * 60),
		  'minutes' => (self.time % (60 * 60))/60,
		  'seconds' => (self.time % (60 * 60)) % 60,
		  'runner' => self.runner,
		  'race' => self.race,
		  'event' => self.race.event,
		  'overallplace' => Result.count(:time.lt => self.time, :race => self.race) + 1,
		  'sexplace' => Result.count(:time.lt => self.time, :race => self.race, Result.runner.sex => self.runner.sex) + 1,
		  'paceminutes' => ((self.time / (self.race.distance / 1000)) % (60 * 60))/60,
		  'paceseconds' => ((self.time / (self.race.distance / 1000)) % (60 * 60))%60
		}.to_json(*a)
	end 
end

class Club
  	include DataMapper::Resource

  	property :id,        	Serial    		
  	property :name,  			String  		
	property :shortname,  	String  
	
end

class Event
  	include DataMapper::Resource

  	property :id,        	Serial    		
  	property :name,  			String  		

	has n, :races

	def to_json(*a)
		{
		  'event_id' => self.id,
		  'name' => self.name
		}.to_json(*a)
	end 
end

class Race
  	include DataMapper::Resource

  	property :id,        	Serial    		
	property :date,			Date
	property :location,		String
	property :distance,		Integer
		
	belongs_to :event
	has n, :results
	has n, :runners, :through => :results
	belongs_to :club, :required => false

	def to_json(*a)
		{
		  'race_id' => self.id,
		  'year' => self.date.year,
		  'month' => self.date.mon,
		  'day' => self.date.day,
		  'location' => self.location,
		  'distance' => self.distance,
		  'event_id' => self.event.id,
		  'eventname' => self.event.name
		}.to_json(*a)
	end 

end

DataMapper.finalize
DataMapper.auto_upgrade!

def addsampledata

	# add some sample data
	ane = Club.first_or_create(:name => 'Athletics North-East', :shortname => 'ANE')
	naut = Club.first_or_create(:name => 'Nautilus Running Club', :shortname => 'NAUT')

	event1 = Event.first_or_create(:name => 'Flat Out 5km Road Race');
	race1 = Race.first_or_create(:date => Date.new(2011,2,3), :distance => 5, :location => 'Torbay', :club => ane, :event => event1);
	race2 = Race.first_or_create(:date => Date.new(2012,2,10), :distance => 5, :location => 'Torbay', :club => ane, :event => event1);

	runner1 = Runner.first_or_create(:sex => :M, :dob => Date.new(1976,03,22))
	runnername1 = Runnername.first_or_create(:firstname => 'Robert', :lastname => 'White', :runner=>runner1)

	runner2 = Runner.first_or_create(:sex => :F, :dob => Date.new(1975,10,26))
	runnername2 = Runnername.first_or_create(:firstname => 'Andrea', :lastname => 'Sparkes', :runner=>runner2)

	runner3 = Runner.first_or_create(:sex => :M, :dob => Date.new(1955,03,26))
	runnername3 = Runnername.first_or_create(:firstname => 'George', :lastname => 'White', :runner=>runner3)

	runner4 = Runner.first_or_create(:sex => :M, :dob => Date.new(1965,9,23))
	runnername4 = Runnername.first_or_create(:firstname => 'Lance', :lastname => 'Sparkes', :runner=>runner4)
	
	runner5 = Runner.first_or_create(:sex => :F, :dob => Date.new(1965,01,11))
	runnername5 = Runnername.first_or_create(:firstname => 'Donna', :lastname => 'Sparkes', :runner=>runner5)

	runner6 = Runner.first_or_create(:sex => :M, :dob => Date.new(1976,04,9))
	runnername6 = Runnername.first_or_create(:firstname => 'Scott', :lastname => 'Kelly', :runner=>runner6)

	Result.first_or_create(:runner => runner1, :bibnumber => 23, :time => 1005, :race=> race1)
	Result.first_or_create(:runner => runner2, :bibnumber => 232, :time => 1034, :race=> race1)
	Result.first_or_create(:runner => runner3, :bibnumber => 290, :time => 1410, :race=> race1)
	Result.first_or_create(:runner => runner4, :bibnumber => 218, :time => 1590, :race=> race1)
	Result.first_or_create(:runner => runner5, :bibnumber => 18, :time => 1620, :race=> race1)
	Result.first_or_create(:runner => runner6, :bibnumber => 13, :time => 1220, :race=> race1)

end












