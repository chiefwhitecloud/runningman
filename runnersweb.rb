# runnersweb.rb
require 'rubygems'
require 'sinatra'
require 'json'
require 'lib/runners'

# Add new runner
# curl -X POST 'http://localhost:4567/runner' -H "Content-Type: application/json" -d '{"sex":"M","firstname":"Bob","lastname":"Black"}' 
post '/runner' do
 	content_type :json
	data = JSON.parse(request.env["rack.input"].read)

	if data.nil? or !data.has_key?('sex') or !data.has_key?('firstname') or !data.has_key?('lastname') then
    status 400
  	else
	 runner = Runner.create( 
    	:sex => data['sex']
    )
    runner.save

	 runnername = Runnername.create(
			:firstname => data['firstname'],
			:lastname => data['lastname'],
			:runner => runner
	 )
	 runnername.save
		
    status 201
    return runner.to_json
	end
end

# Add runner name to an existing runner
# curl -X POST 'http://localhost:4567/runner/1/name' -H "Content-Type: application/json" -d '{"firstname":"Bob","lastname":"Black"}' 
get '/runner/:id/results' do

	runner = Runner.get(params[:id])
 	if runner.nil?
 		halt 400
 	end

	@results = runner.results

	format_list(@results)

end




# Add runner name to an existing runner
# curl -X POST 'http://localhost:4567/runner/1/name' -H "Content-Type: application/json" -d '{"firstname":"Bob","lastname":"Black"}' 
post '/runner/:id/name' do
 	content_type :json
	data = JSON.parse(request.env["rack.input"].read)

	if data.nil? or !data.has_key?('firstname') or !data.has_key?('lastname') then
    status 400
  	else
	 runner = Runner.get(params[:id])
	 if runner.nil?
		halt 400
	 end 

	 runnername = Runnername.create(
			:firstname => data['firstname'],
			:lastname => data['lastname'],
			:runner => runner
	 )
	 runnername.save
		
    status 201
    return runner.to_json
	end
end


# Get a list of all runners
# Can include first or last names on the querystring
get '/runners' do
 	content_type :json

	if params[:firstname] && params[:lastname]
		runners = Runner.all(Runner.runnernames.firstname => params[:firstname], Runner.runnernames.lastname => params[:lastname])
	elsif params[:firstname]
		runners = Runner.all(Runner.runnernames.firstname => params[:firstname])
	elsif params[:lastname]
		runners = Runner.all(Runner.runnernames.lastname => params[:lastname]) 
	else
		runners = Runner.all()	
	end	
	
	format_list(runners)
end


# return an array of results for the race 
get '/race/:id/results' do
	content_type :json
	results = Race.get(params[:id]).results(:order => [ :time.asc ])
	format_list(results)
end


# Add result to the race
# curl -X POST 'http://localhost:4567/race/1/result' -H "Content-Type: application/json" -d '{"runner-id":"1","bib":"40", "time-hours":"40", "time-minutes":"40", "time-seconds":"30"}'
post '/race/:id/result' do
	content_type :json
	race = Race.get(params[:id])
	
	if race.nil?
		halt 400
	end 

	data = JSON.parse(request.env["rack.input"].read)

	#check that the posted json has the right keys
	if data.nil? or !data.has_key?('runner-id') or !data.has_key?('bib') or !data.has_key?('time-hours') or !data.has_key?('time-minutes') or !data.has_key?('time-seconds') then
    	status 400
  	else

		#fetch the runner
		runner = Runner.get(data['runner-id'])

		if runner.nil?
			halt 400
		end 

		#check if the runner already has an entry for this race.
		exists = Result.get(:runner => runner, :race => race)
		
		if !exists.nil?
			halt 400
		end 
		
		#create the new result
		# the time is stored in seconds
		re = Result.create(
			:time => (data['time-hours'].to_i * 3600) + (data['time-minutes'].to_i * 60) + data['time-seconds'].to_i,
			:bibnumber => data['bib'],
			:runner => runner,
			:race => race)

		re.save

		return re.to_json

	end
end



get '/races' do
	content_type :json
	format_list(Race.all(:order => [ :date.desc ]))
end

get '/events' do
	content_type :json
	format_list(Event.all(:order => [ :name.asc ]))
end


# return a race
get '/race/:id' do
  content_type :json
 
  race = Race.get(params[:id])
  if race.nil?
    halt 400
  end

  race.to_json

end


# return a single event
get '/event/:id' do
	content_type :json

	event = Event.get(params[:id])
	if event.nil?
		halt 400
	end 
	
	event.to_json

end

# return a list of races
get '/event/:id/races' do
	content_type :json
	
	event = Event.get(params[:id])
	if event.nil?
		halt 400
	end 

	results = event.races(:order => [ :date.desc ])
	format_list(results)

end


# Add new event
# curl -X POST 'http://localhost:4567/event' -H "Content-Type: application/json" -d '{"name":"Tely 10"}' 
post '/event' do
 	content_type :json
	data = JSON.parse(request.env["rack.input"].read)

	if data.nil? or !data.has_key?('name') then
    status 400
  	else
	 event = Event.create( 
    	:name => data['name']
    )
    event.save
    status 201
    return event.to_json
	end
end



# Add race to an existing event
# curl -X POST 'http://localhost:4567/event/1/race' -H "Content-Type: application/json" -d '{"location":"Torbay","distance":"10","year":"2010","month":"10", "day":"1"}' 
post '/event/:id/race' do
 	content_type :json
	data = JSON.parse(request.env["rack.input"].read)

	if data.nil? or !data.has_key?('location') or !data.has_key?('distance') or !data.has_key?('year') or !data.has_key?('month') or !data.has_key?('day') then
    status 400
  	else
	 event = Event.get(params[:id])
	 if event.nil?
		halt 400
	 end 

	 race = Race.create(
			:location => data['location'],
			:distance => data['distance'].to_i,
			:date => Date.new(data['year'].to_i, data['month'].to_i, data['day'].to_i),
			:event => event
	 )
	 race.save
		
    status 201
    return race.to_json
	end
end



def format_list(results)
	str = "["
	results.each_with_index do |result, index|
		str << result.to_json
		if index < results.size - 1
			str << ","
		end
	end 
	return str << "]"
end



