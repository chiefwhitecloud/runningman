Running Man Data API
====================

All POST requests *should* return response code 201 and the json for the created object.

## ADD: new runner

__Method:__ POST

__Route:__ /runner

__JSON Structure__

+ sex: "M" or "F"
+ firstname: string
+ lastname: string

__sample__ : curl -X POST 'http://localhost:4567/runner' -H "Content-Type: application/json" -d '{"sex":"M","firstname":"Bob","lastname":"Black"}'


## ADD: runner name to an existing runner

__Method:__ POST

__Route:__ /runner/ **[runner_id]** /name

__JSON structure__

+ firstname: string
+ lastname: string

__sample__ : curl -X POST 'http://localhost:4567/runner/1/name' -H "Content-Type: application/json" -d '{"firstname":"Bob","lastname":"Black"}'




## ADD: runner result

__Method:__ POST

__Route:__ /runner/ **[race_id]** /result

__JSON Structure__

+ runner-id: int
+ bib: string
+ time-hours: string
+ time-minutes: string
+ time-seconds: string

__sample__ : curl -X POST 'http://localhost:4567/race/1/result' -H "Content-Type: application/json" -d '{"runner-id":"1","bib":"40", "time-hours":"40", "time-minutes":"40", "time-seconds":"30"}'



## ADD: event 
__Method:__ POST

__Route:__ /event

__JSON Structure__

+ name: string

__sample__ : curl -X POST 'http://localhost:4567/event' -H "Content-Type: application/json" -d '{"name":"Tely 10"}' 


## ADD: race to event

__Method:__ POST

__Route:__ /event/ **[event_id]** /race

__JSON Structure__

+ location: string
+ distance: int
+ year: int
+ month: int
+ day: int

__sample__ : curl -X POST 'http://localhost:4567/event/1/race' -H "Content-Type: application/json" -d '{"location":"Torbay", "distance":"10", "year":"2010", "month":"10", "day":"1"}' 



## GET: all runners

__Method:__ GET

__Route:__ /runners

__Route Querystring Options:__ 

+ firstname
+ lastname


__sample__ : curl 'http://localhost:4567/runners?firstname=robert'



## GET: all results for a race

__Method:__ GET

__Route:__ /race/ **[race_id]** /results


__sample__ : curl 'http://localhost:4567/race/1/results'






